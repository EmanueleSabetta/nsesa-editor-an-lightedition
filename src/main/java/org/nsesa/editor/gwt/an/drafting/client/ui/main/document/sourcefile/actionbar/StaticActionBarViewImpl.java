/**
 * Copyright 2013 European Parliament
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package org.nsesa.editor.gwt.an.drafting.client.ui.main.document.sourcefile.actionbar;

import org.nsesa.editor.gwt.core.client.ui.custom.PeoButton;
import org.nsesa.editor.gwt.core.client.ui.document.sourcefile.actionbar.ActionBarView;
import org.nsesa.editor.gwt.core.client.ui.overlay.TextUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;
import com.google.inject.Inject;
import org.nsesa.editor.gwt.core.client.ui.document.sourcefile.actionbar.ActionBarView;
import org.nsesa.editor.gwt.core.client.ui.overlay.TextUtils;

/**
 * Date: 10/04/13 23:12
 * 
 * @author <a href="mailto:philip.luppens@gmail.com">Philip Luppens</a>
 * @version $Id$
 */
public class StaticActionBarViewImpl extends Composite implements ActionBarView {
    interface MyUiBinder extends UiBinder<Widget, StaticActionBarViewImpl> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    /**
     * Vincenzo Abate
     * 12/12/2013
     */
    // @UiField
    // Anchor modifyAnchor;
    // @UiField
    // Anchor bundleAnchor;
    // @UiField
    // Anchor newAnchor;
    // @UiField
    // Anchor deleteAnchor;
    // @UiField
    // Anchor moveAnchor;
    // @UiField
    // Anchor translateAnchor;
    @UiField
    PeoButton modifyButton;
    @UiField
    Button bundleButton;
    @UiField
    PeoButton childButton;
    @UiField
    PeoButton deleteButton;
    @UiField
    PeoButton moveButton;
    @UiField
    Button translateButton;
    @UiField
    Label location;
    @UiField
    /**
     * Vincenzo Abate
     * 12/12/2013
     */
    // HTMLPanel actionPanel;
    VerticalPanel actionPanel;

    @Inject
    public StaticActionBarViewImpl() {
        final Widget widget = uiBinder.createAndBindUi(this);
        initWidget(widget);
        if (!GWT.isScript())
            widget.setTitle(this.getClass().getName());
    }

    @Override
    public void addWidget(IsWidget isWidget) {
        actionPanel.add(isWidget);
    }

    /**
     * Vincenzo Abate 12/12/2013
     */
    // @Override
    // public ComplexPanel getActionPanel() {
    // return actionPanel;
    // }
    @Override
    public VerticalPanel getActionPanel() {
        return actionPanel;
    }

    @Override
    public FocusWidget getModifyHandler() {
//        return modifyAnchor;
        return modifyButton;
    }

    @Override
    public FocusWidget getDeleteHandler() {
//        return deleteAnchor;
        return deleteButton;
    }

    @Override
    public FocusWidget getBundleHandler() {
//        return bundleAnchor;
        return bundleButton;
    }

    @Override
    public FocusWidget getMoveHandler() {
//        return moveAnchor;
        return moveButton;
    }

    @Override
    public FocusWidget getChildHandler() {
//        return newAnchor;
        return childButton;
    }

    @Override
    public FocusWidget getTranslateHandler() {
//        return translateAnchor;
        return translateButton;
    }

    @Override
    public void setLocation(String location) {
        if (location != null) {
            this.location.setText(TextUtils.capitalize(location));
        } else {
            this.location.setText("");
        }
    }

    @Override
    public void attach() {
        onAttach();
    }
}
